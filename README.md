# solar/common

This repository contains files that are required for common tasks/automation
(such as continuous deployments or some aspect of continuous integration).

## Contents

* `nomad-ca.pem`: CA certificate in PEM format used to verify signature of
  solar.coala.io Nomad cluster. This file is required to do automated
  deployments to solar via Nomad.
